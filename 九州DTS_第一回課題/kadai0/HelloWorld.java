package kadai0;

/**
 * 文字列"Hello World!!"をコンソールに出力する.
 * @author 鈴木崇史
 */
public class HelloWorld {
		/**
	 * Hello World!!出力.
	 * <p>
	 * コンソールに"Hello World!!"と出力する。
	 * </p>
	 */
    public static void main (String[] args) {
        System.out.println("Hello World !!");
    }
}