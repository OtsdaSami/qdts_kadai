package kadai2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * テキストファイルを読み込んで、検索ワードを含む項目数をカウントして標準出力する.
 * @author 鈴木崇史
 */
public class Kadai2 {
	/**
	 * 検索ワード出現数出力.
	 * <p>
	 * テキストファイルを読み込んで、検索ワードを含む項目数をカウントして標準出力する。
	 * </p>
	 *
	 * @param args[0]	起動引数１．テキストファイル名（フルパス）	（null不可）
	 * @param args[1]	起動引数２．検索ワード				（null不可）
	 * @throws java.io.IOException パスの間違い、ファイルが存在しなかった場合の例外
	 */
	public static void main(String[] args) throws IOException {

		// 検索ワードを含む項目数の出力用
		int		searchResults 	= 0;
		// 検索されるファイルとsearchFileの紐付け用
		String 	filePass 		= null;
		// 検索ワードの保持用
		String 	searchWord 		= null;
		// 文字列検索のためにテキストファイルをString型にして保持する変数
		String 	searchString 	= null;
		// 検索ワードのある行を[]で括り行番号と共に抜き出した文字列の保持用
		String	markReplacedString = null;
		// 検索するテキストファイル紐付け用
		File 	searchFile 		= null;


		// 引数の例外とファイル読み込み時の例外を検出する
		try {
			filePass 	= args[0];
			searchWord 	= args[1];

			searchFile	= new File(filePass);

		}catch(Exception e) {
			System.out.println("エラーが発生しました。：" + e);
			return;

		}

		// 検索して表示
		searchString 	= fileConvertString(searchFile);
		searchResults 	= cntSearchWord(searchString, searchWord);
		System.out.println(searchWord + "=" + searchResults + "件");

		// 検索ワードの該当箇所を強調して、行番号と共に出力
		markReplacedString = markStringPull(searchString, searchWord);
		if(markReplacedString != null){
			System.out.println(markReplacedString);
		}else{
			System.out.println("検索ワードの値が不正です。");
		}
	}


	/**
	 * テキストファイルString型変換.
	 * <p>
	 * テキストファイルをString型に変換して返す。<br>
	 * テキストが読み込めない場合は'null'を返す。
	 * </p>
	 *
	 * @param convertFile 	String型にしたいテキストファイル（null不可）
	 * @return 変換後のテキスト								（ファイルが読み込めない場合は'null'）
	 * @throws java.io.IOException ファイルから文字列が読み込めなかった場合の例外
	 */
	public static String fileConvertString(File convertFile) throws IOException {

		// 引数のnullチェック。コードのネストを浅くするために独立させた
		if(convertFile == null){
			return null;
		}

		BufferedReader bufferedReader = null;
		try {
			// テキストファイルの読み込みに使用する添え字
			int cntReadChar = 0;
			// テキストファイルの内容を代入する変数
			StringBuffer stringBuffer = new StringBuffer();


			// ファイルの内容をString型にする
			bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(convertFile),"SJIS"));
			while ((cntReadChar = bufferedReader.read()) != -1) {
				stringBuffer.append((char) cntReadChar);
			}

			return stringBuffer.toString();

		}catch(Exception e) {
			System.out.println(e);
			return null;

		}finally {
			// バッファを必ず閉じるためにreturn後にも実行されるここに記述
			bufferedReader.close();

		}
	}


	/**
	 * 検索ワード出現回数検出.
	 * <p>
	 * 検索される文字列中の検索ワードを含む項目数をカウントして返す。<br>
	 * 検索される文字列・検索ワードが空文字列、又はnullの場合は 0 を返す。
	 * </p>
	 *
	 * @param findString 	検索される文字列（null可）
	 * @param findWord 		検索ワード		（null可）
	 * @return 検出された項目数				（必ず0以上）
	 */
	public static int cntSearchWord(String findString, String findWord){

		// 検索ワードを含む項目数
		int cntFindWord 	= 0;
		// 検索される文字列の長さ
		int findStringSize 		= 0;
		// 検索される文字列から検索ワードを引いた長さ
		int replacedFindStringSize 	= 0;


		// 検出数を検出する、検索ワードが'null'又は空文字列の場合は検出数０とする
		findStringSize = findString.length();
		replacedFindStringSize = findString.replaceAll(findWord,"").length();
		try{
			cntFindWord = (findStringSize-replacedFindStringSize)/findWord.length();
		}catch(ArithmeticException e){
			cntFindWord = 0;
		}

		return cntFindWord;

	}


	/**
	 * 検索ワードを強調＆行番号で抜き出し.
	 * <p>
	 * 検索される文字列中にある検索ワードを [] で括り、行番号と付与して返す。<br>
	 * 検索ワードが空文字列、又はnullの場合は'null'を返す。
	 * </p>
	 * @param markString	検索される文字列	（null可）
	 * @param markWord		[]で括る検索ワード	（null・空文字列不可）
	 * @return 検索ワードを[]で括り、行番号を付与して該当行を抜き出した文字列	（又は 'null'）
	 */
	public static String markStringPull(String markString, String markWord){

		// 検索ワードが'null'又は空文字列の場合、置き換え処理が正常に動作しないため最初に判別する
		if( markWord == null || markWord.length() <= 0){
			return null;
		}

		// 検索する行番号
		int findLineNumber = 1;
		// 戻り値用
		String replacedString = "";
		// 検索される文字列を分割して保持する変数
		String[] searchLines = null;


		//置き換え処理
		searchLines = markString.split("(\\r\\n|\\r|\\n)");
		for(String splitLine : searchLines){
			if(splitLine.matches(".*" + markWord + ".*")){
				splitLine = splitLine.replace("^", String.valueOf(findLineNumber));
				splitLine = splitLine.replace(markWord,"[" + markWord + "]");
				replacedString += findLineNumber + ":" + splitLine +"\n";

			}
			findLineNumber++;

		}
		//上記の処理では文字列の末尾に改行（\n）が追加されるので、末尾の改行を削除する
		replacedString = replacedString.replaceAll("$[^.]" , "");
		return replacedString;

	}
}