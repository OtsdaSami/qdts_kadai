package kadai3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * テキストファイルを読み込んで、検索ワードを含む項目数をカウントして標準出力する.
 * @author 鈴木崇史
 */
public class Kadai3 {
	/**
	 * 検索ワード出現数出力.
	 * <p>
	 * テキストファイルを読み込んで、検索ワードを含む項目数をカウントして標準出力する。
	 * </p>
	 *
	 * @param args[0]	起動引数１．テキストファイル名（フルパス）	（null不可）
	 * @param args[1]	起動引数２．検索ワード				（null不可）
	 * @throws java.io.IOException パスの間違い、ファイルが存在しなかった場合の例外
	 */
	public static void main(String[] args) throws IOException {

		// 検索ワードを含む項目数の出力用
		int		searchResults 	= 0;
		// 検索されるファイルとsearchFileの紐付け用
		String 	filePass 		= null;
		// 検索ワードの保持用
		String 	searchWord 		= null;
		// 文字列検索のためにテキストファイルをString型にして保持する変数
		String 	searchString 	= null;
		// 検索ワードのある行を[]で括り行番号と共に抜き出した文字列の保持用
		String	markReplacedString = null;
		// 検索するテキストファイル紐付け用
		File 	searchFile 		= null;


		// 引数の例外とファイル読み込み時の例外を検出する
		try {
			filePass 	= args[0];
			searchWord 	= args[1];

			searchFile	= new File(filePass);

		}catch(Exception e) {
			System.out.println("エラーが発生しました。：" + e);
			return;

		}

		// 検索して表示
		searchString 	= fileConvertString(searchFile);
		searchResults 	= cntSearchWord(searchString, searchWord);
		System.out.println(searchWord + "=" + searchResults + "件");

		// 検索ワードの該当箇所を強調して、行番号と共に出力
		markReplacedString = String.valueOf(markStringPull(searchString.toCharArray(), searchWord.toCharArray()));
		if(markReplacedString != null){
			System.out.println(markReplacedString);
		}else{
			System.out.println("検索ワードの値が不正です。");
		}
	}


	/**
	 * テキストファイルString型変換.
	 * <p>
	 * テキストファイルをString型に変換して返す。<br>
	 * テキストが読み込めない場合は'null'を返す。
	 * </p>
	 *
	 * @param convertFile 	String型にしたいテキストファイル（null不可）
	 * @return 変換後のテキスト								（ファイルが読み込めない場合は'null'）
	 * @throws java.io.IOException ファイルから文字列が読み込めなかった場合の例外
	 */
	public static String fileConvertString(File convertFile) throws IOException {

		// 引数のnullチェック。コードのネストを浅くするために独立させた
		if(convertFile == null){
			return null;
		}

		BufferedReader bufferedReader = null;
		try {
			// テキストファイルの読み込みに使用する添え字
			int cntReadChar = 0;
			// テキストファイルの内容を代入する変数
			StringBuffer stringBuffer = new StringBuffer();


			// ファイルの内容をString型にする
			bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(convertFile),"SJIS"));
			while ((cntReadChar = bufferedReader.read()) != -1) {
				stringBuffer.append((char) cntReadChar);
			}

			return stringBuffer.toString();

		}catch(Exception e) {
			System.out.println(e);
			return null;

		}finally {
			// バッファを必ず閉じるためにreturn後にも実行されるここに記述
			bufferedReader.close();

		}
	}


	/**
	 * 検索ワード出現回数検出.
	 * <p>
	 * 検索される文字列中の検索ワードを含む項目数をカウントして返す。<br>
	 * 検索される文字列・検索ワードが空文字列、又はnullの場合は 0 を返す。
	 * </p>
	 *
	 * @param findString 	検索される文字列（null可）
	 * @param findWord 		検索ワード		（null可）
	 * @return 検出された項目数				（必ず0以上）
	 */
	public static int cntSearchWord(String findString, String findWord){

		// 検索ワードを含む項目数
		int cntFindWord 	= 0;
		// 検索される文字列の長さ
		int findStringSize 		= 0;
		// 検索される文字列から検索ワードを引いた長さ
		int replacedFindStringSize 	= 0;


		// 検出数を検出する、検索ワードが'null'又は空文字列の場合は検出数０とする
		findStringSize = findString.length();
		replacedFindStringSize = findString.replaceAll(findWord,"").length();
		try{
			cntFindWord = (findStringSize-replacedFindStringSize)/findWord.length();
		}catch(ArithmeticException e){
			cntFindWord = 0;
		}

		return cntFindWord;

	}


	/**
	 * 検索ワードを強調＆行番号で抜き出し.
	 * <p>
	 * 検索される文字列中にある検索ワードを [] で括り、行番号と付与してchar[]型で返す。<br>
	 * 検索ワードが空文字列、又はnullの場合は'null'を返す(文字型配列ではない)。
	 * </p>
	 * @param markChars	検索される文字列	（null可）
	 * @param markWordChars		[]で括る検索ワード	（null・空文字列不可）
	 * @return 検索ワードを[]で括り、行番号を付与して該当行を抜き出したchar型配列	（又は 'null'）
	 */
	public static char[] markStringPull(char[] markChars, char[] markWordChars){

		//検索される文字配列の長さ
		int markCharsSize = 0;
		//検索ワードの文字配列の長さ
		int markWordSize = 0;
		//検索される文字配列の添え字
		int cntCharsNumber = 0;
		//検索ワードの文字配列の添え字
		int cntWordNumber = 0;
		//置き換え後の文字を格納する文字配列の添え字
		int cntReplacedNumber = 0;
		//検索している行の行頭を表す添え字
		int startLineNumber = 0;
		//検索している行番号
		int lineNumber = 1;
		//trueの場合は、検索した行に検索ワードが含まれていることを表している
		boolean containsFindLine = false;

		//文字配列の長さ取得
		markCharsSize = countCharsSize(markChars);
		markWordSize = countCharsSize(markWordChars);

		// 検索ワードが'null'又は空文字列の場合、置き換え処理が正常に動作しないため早めに判別する
		if( markWordChars == null || markWordSize == 0){
					return null;
		}

		//置き換え後の文字を格納する文字配列。置き換え時の文字量増加対策に配列数を多めに取っている
		char[] replacedChars = new char[markCharsSize * 2];


		//検索される文字配列中にある検索ワードを [] で括り、行番号と付与してreplacedCharsに格納する。
		//markChars[]の添え字は、改行コード等を避けるためにreplacedChars[]の添え字と独立しているので注意
		while(markCharsSize > cntCharsNumber){
			//行末まで検索終了すると、行中に検索ワードが含まれているかを確認し、次の行の検索に進む
			if(markChars[cntCharsNumber] == '\r'){
				if(containsFindLine){
					replacedChars[cntReplacedNumber] = '\n';
					cntReplacedNumber++;
					containsFindLine =false;
					startLineNumber = cntReplacedNumber;

				}else{
					cntReplacedNumber = startLineNumber;
				}
				cntCharsNumber += 2;
				lineNumber++;

			}
			//新しい行の検索開始時に、行頭に行番号を追加する
			if(startLineNumber == cntReplacedNumber){
				replacedChars[cntReplacedNumber] = (char)('0' + lineNumber);
				cntReplacedNumber++;
				replacedChars[cntReplacedNumber] = ':';
				cntReplacedNumber++;
			}
			//検索される文字配列と検索ワードを比較して、検索ワードと完全一致すれば、[]で括る
			if(markWordSize > cntWordNumber && markChars[cntCharsNumber] == markWordChars[cntWordNumber]){
				replacedChars[cntReplacedNumber] = '[';
				cntReplacedNumber++;
				while(markWordSize > cntWordNumber){
					replacedChars[cntReplacedNumber] = markChars[cntCharsNumber];
					cntCharsNumber++;
					cntWordNumber++;
					cntReplacedNumber++;
				}
				if(markWordSize <= cntWordNumber){
					containsFindLine = true ;
					replacedChars[cntReplacedNumber] = ']';
					cntReplacedNumber++;
				}

			}else{
				cntWordNumber = 0;

				replacedChars[cntReplacedNumber] = markChars[cntCharsNumber];
				cntCharsNumber++;
				cntReplacedNumber++;

			}
		}

		//余分な空文字配列を削除する
		replacedChars = kerningblankChars(replacedChars, cntReplacedNumber);

		return replacedChars;
	}

	/**
	 * 配列数計算
	 * <p>
	 * 引数で受け取った文字配列の配列数を整数型で返す。
	 * </p>
	 *
	 * @param calcChars 配列数を検索する文字配列	(null不可)
	 * @return 文字配列の配列数	（必ず0以上）
	 */
	public static int countCharsSize(char[] calcChars){

		int cntCharsSize = 0;

		for (char charsLine : calcChars) {
			cntCharsSize++;
		}

		return cntCharsSize;
	}


	/**
	 * 配列数変更
	 * <p>
	 * 文字配列の配列数をcharsSizeに変更する。<br>
	 * charasSize以上の配列に格納されていたデータは消えるので注意
	 * </p>
	 *
	 * @param kerningChars 配列数を変更する文字配列	（null不可）
	 * @param charsSize 変更後の配列数	(0以上)
	 * @return 配列数が変更された文字配列。末尾より前のデータは保持されたまま （配列数0以上）
	 */
	public static char[] kerningblankChars(char[] kerningChars, int charsSize){

		char[] returnChars = new char[charsSize];
		int cntCharSize = 0;

		while(charsSize > cntCharSize){
			returnChars[cntCharSize] = kerningChars[cntCharSize];
			cntCharSize++;
		}

		return returnChars;
	}
}